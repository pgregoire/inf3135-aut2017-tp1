.POSIX:

include config.mk


BINS=	bin/generator bin/pandemic
SRC=	src/generator.c src/pandemi.c
OBJ=	${SRC:.c=.o}


all: ${BINS}

check:
	sh ./tests.sh

clean:
	rm -f ${BINS} ${OBJ}
	[ ! -d bin ] || rmdir bin
	rm -rf tests_out


bin/generator: src/generator.o
	[ -d bin ] || mkdir -p bin
	${LD} ${LDFLAGS} -o bin/generator src/generator.o

bin/pandemic: src/pandemi.o
	[ -d bin ] || mkdir -p bin
	${LD} ${LDFLAGS} -o bin/pandemic src/pandemi.o

src/generator.o: src/generator.c
	${CC} ${CPPFLAGS} ${CFLAGS} -c -o src/generator.o src/generator.c
