
CC=	gcc
LD=	${CC}

CFLAGS=		-Wall -Werror -std=c11 -pedantic
LDFLAGS=	-Wl,-z,noexecstack -Wl,-z,now -Wl,-z,relro
CPPFLAGS=
