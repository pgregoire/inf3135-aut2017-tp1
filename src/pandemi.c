/**
 * Simulateur d'evolution d'une pandemie.
 *
 * INF3135 - Automne 2017 - Travail Pratique 1
 *
 *
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* ========================================================================== */

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* -------------------------------------------------------------------------- */

/* les dimensions d'une carte */
#define CARTE_LARGEUR	40
#define CARTE_HAUTEUR	20
#define CARTE_AIRE	(CARTE_LARGEUR * CARTE_HAUTEUR)

/* les marqueurs de type de population */
#define DEMO_SAINE	'H'
#define DEMO_VIRUS	'X'
#define DEMO_VIDE	'.'

#define JOURS_MAX	100

#define VOISIN_HAUTGAU	1
#define VOISIN_HAUT	2
#define VOISIN_HAUTDROI	3
#define VOISIN_GAUCHE	4
#define VOISIN_DROIT	5
#define VOISIN_BASGAU	6
#define VOISIN_BAS	7
#define VOISIN_BASDROI	8
#define NVOISINS	8

/* -------------------------------------------------------------------------- */

/**
 * Affiche un message d'erreur sur stderr.
 *
 * Il s'agit, essentiellement, d'une fonction qui affiche
 * @fmt et ses arguments sur stderr avec les modifications
 * suivantes:
 *
 *   - "Erreur: " est imprimee avant la chaine en argument;
 *   - si fmt se termine par ':', un message decrivant errno est affiche;
 *   - si fmt ne se termine par par '\n', celui-ci est ajoute.
 *
 *
 * @param  fmt    la chaine de formattage de la sortie, voir printf(3)
 * @param  ...    les valeurs à inserer dans la sortie
 * @return        ne retourne rien
 */
static void eprintf(const char* fmt, ...)
{
	char ch;
	va_list vl;

	assert(0 != fmt);


	fprintf(stderr, "Erreur: ");

	va_start(vl, fmt);
	vfprintf(stderr, fmt, vl);
	va_end(vl);

	ch = fmt[strlen(fmt) - 1];
	if (':' == ch) {
		fprintf(stderr, " %s\n", strerror(errno));
	} else if ('\n' != ch) {
		putc('\n', stderr);
	}
}

/**
 * Affiche que @ch est un caractere invalide.
 *
 * Affiche un message d'erreur indiquant que @ch n'est
 * pas un caractere valide pour une carte.
 *
 *
 * @param  ch    le caractere invalide
 * @return       ne retourne rien
 */
static void err_invalch(char ch)
{
	char s[4];

	if (EOF != ch) {
		s[0] = ch;
		s[1] = '\0';
	} else {
		strcpy(s, "EOF");
	}

	eprintf("Caractère `%s` inattendu, attendu `%c`, `%c` ou `%c`.",
			s, DEMO_SAINE, DEMO_VIRUS, DEMO_VIDE);
}

/* -------------------------------------------------------------------------- */

/**
 * Affiche la carte sur la sortie standard.
 *
 * Affiche la carte contenue dans @cartep sur la sortie standard.
 *
 *
 * @param  cartep    la carte de simulation a afficher
 * @return           ne retourne rien
 */
static void afficher_carte(const char* cartep)
{
	size_t i;

	assert(0 != cartep);


	for (i = 0 ; CARTE_AIRE > i ; i++) {
		if ((0 != i) && (0 == (i % CARTE_LARGEUR))) {
			putchar('\n');
		}
		putchar(cartep[i]);
	}

	putchar('\n');
}

/**
 * Affiche le jour suivi de la carte.
 *
 * Affiche la carte @cartep pour le jour @journum.
 * Le jour est affiche en premier, suivi de la carte.
 *
 *
 * @param  journum    le jour que la carte represente
 * @param  cartep     la carte de simulation a afficher
 * @return            ne retourne rien
 */
static void afficher_jourcarte(size_t journum, const char* cartep)
{
	assert(0 != cartep);

	printf("Jour %zu\n", journum);
	afficher_carte(cartep);
}

/* -------------------------------------------------------------------------- */

/**
 * Determine si un caractere est valide dans un carte.
 *
 * Retourne non-zero si @ch est un caractere valide pour
 * une carte de simulation.
 *
 *
 * @param  ch    le caractere a evaluer
 * @return       non-zero si @ch est valide dans une carte
 */
static int ch_isvalid(int ch)
{
	switch (ch) {
	case DEMO_SAINE: /* on tombe */
	case DEMO_VIRUS: /* on tombe */
	case DEMO_VIDE:
		return 1;
	default:
		break;
	}

	errno = EINVAL;
	return 0;
}

/**
 * Lis une carte de simulation
 *
 * Lis une carte de simulation depuis @fp et
 * l'enregistre dans @cartep.
 *
 *
 * @param  fp        le fichier a lire
 * @param  cartep    le tampon de destination de la carte
 * @return           0 en cas de succes
 */
static int lire_carte(FILE* fp, char* cartep)
{
	char ch;
	size_t i;

	assert((0 != fp) && (0 != cartep));


	for (i = 0 ; CARTE_AIRE > i ; /**/) {
		ch = fgetc(fp);
		if (0 != isspace(ch)) {
			continue;
		} else if (0 == ch_isvalid(ch)) {
			err_invalch(ch);
			return -1;
		}

		cartep[i++] = ch;
	}

	return 0;
}

/* -------------------------------------------------------------------------- */

/**
 * Determine si une cellule est a une bordure de la carte.
 *
 * Détermine si la cellule @cellnum est a la bordure @type de la carte.
 *
 * Retourne non-zero, si c'est le cas.
 *
 *
 * @param  cellnum    l'index de la cellule
 * @param  type       le type de bordure
 * @return            non-zero si la cellule est au bordure de la carte
 */
static int cell_islim(size_t cellnum, int type)
{
	switch (type) {
	case 0: /* haut */
		return (CARTE_LARGEUR > cellnum);
	case 1: /* droit */
		return (0 == ((cellnum + 1) % CARTE_LARGEUR));
	case 2: /* bas */
		return ((CARTE_AIRE - CARTE_LARGEUR) <= cellnum);
	case 3: /* gauche */
		return (0 == (cellnum % CARTE_LARGEUR));
	default:
		break;
	}

	eprintf("erreur interne - cell_islim: %i", type);
	exit(1);
}

/**
 * Calcule la position d'une cellule voisine
 *
 * Calcule la position du voisin @voisin de @cellnum.
 *
 * Retourne la position du voisin ou -1 si le voisin n'existe pas.
 *
 *
 * @param  cellnum    l'index de la cellule
 * @param  voisin     la direction du voisin
 * @return            la position du voisin, -1 si neant
 */
static int get_posvoisin(int cellnum, int voisin)
{
	if (0 > cellnum) {
		return -1;
	}

	switch (voisin) {
	case VOISIN_HAUT:
		if (cell_islim(cellnum, 0)) {
			return -1;
		}
		return (cellnum - CARTE_LARGEUR);
	case VOISIN_DROIT:
		if (cell_islim(cellnum, 1)) {
			return -1;
		}
		return (cellnum + 1);
	case VOISIN_BAS:
		if (cell_islim(cellnum, 2)) {
			return -1;
		}
		return (cellnum + CARTE_LARGEUR);
	case VOISIN_GAUCHE:
		if (cell_islim(cellnum, 3)) {
			return -1;
		}
		return (cellnum - 1);
	case VOISIN_HAUTGAU:
		return get_posvoisin(get_posvoisin(cellnum, VOISIN_HAUT), VOISIN_GAUCHE);
	case VOISIN_HAUTDROI:
		return get_posvoisin(get_posvoisin(cellnum, VOISIN_HAUT), VOISIN_DROIT);
	case VOISIN_BASGAU:
		return get_posvoisin(get_posvoisin(cellnum, VOISIN_BAS), VOISIN_GAUCHE);
	case VOISIN_BASDROI:
		return get_posvoisin(get_posvoisin(cellnum, VOISIN_BAS), VOISIN_DROIT);
	default:
		break;
	}

	eprintf("erreur interne - get_posvoisin: %i", voisin);
	exit(1);
}

/**
 * Determine le type de voisin d'une cellule
 *
 * Détermine le type du @voisin de la cellule @cellnum dans @cartep.
 *
 * Retourne 0 si le voisin n'existe pas.
 *
 *
 * @param  cartep     la carte de simulation
 * @param  cellnum    l'index de la cellule d'interet
 * @param  voisin     la direction du voisin d'interet
 * @return            le type de population voisine; 0 si neant
 */
static int get_typevoisin(const char* cartep, size_t cellnum, int voisin)
{
	int posvoisin;

	assert(0 != cartep);


	if (0 > (posvoisin = get_posvoisin(cellnum, voisin))) {
		return 0;
	}

	return cartep[posvoisin];
}

/**
 * Compte les voisins d'un type d'une cellule
 *
 * Compte les voisins de @cellnum qui ont le type @type dans @cartep.
 *
 *
 * @param  cartep     la carte de simulation
 * @param  cellnum    l'index de la cellule d'interet
 * @param  type       le type de voisins d'interet
 * @return            le nombre de voisins du type d'interet
 */
static size_t compte_voisins(const char* cartep, size_t cellnum, int type)
{
	size_t v;
	size_t cnt;

	for (v = 1, cnt = 0 ; NVOISINS >= v ; v++) {
		if (get_typevoisin(cartep, cellnum, v) == type) {
			cnt++;
		}
	}

	return cnt;
}

/**
 * Simule l'evolution d'une cellule
 *
 * Retourne l'évolution d'une cellule de type @type
 * ayant les voisins @nvide, @nsains et @nvirus.
 *
 *
 * @param  type      le type courant de la cellule
 * @param  nvide     le nombre de voisins sans population
 * @param  nsains    le nombre de voisins sains
 * @param  nvirus    le nombre de voisins infectes
 * @return           le type de la cellule apres 1 jour
 */
static int simuler_cell(int type, size_t nvide, size_t nsains, size_t nvirus)
{
	switch (type) {
	case DEMO_VIDE:
		if (3 == (nsains + nvirus)) {
			return (nsains > nvirus) ? DEMO_SAINE : DEMO_VIRUS;
		}
		return DEMO_VIDE;
	case DEMO_SAINE: /* on tombe */
	case DEMO_VIRUS:
		if ((2 == (nsains + nvirus)) || (3 == (nsains + nvirus))) {
			return (nvirus > nsains) ? DEMO_VIRUS : type;
		}
		return DEMO_VIDE;
	default:
		break;
	}

	eprintf("erreur interne - simuler_cell: %i", type);
	exit(1);
}

/**
 * Simule un jour d'evolution
 *
 * Simule un jour d'évolution de la population dans @cartep.
 *
 *
 * @param  cartep    la carte de simulation
 * @return           ne retourne rien
 */
static void simuler_jour(char* cartep)
{
	size_t i;
	char type;
	size_t nvide;
	size_t nsains;
	size_t nvirus;

	char newcarte[CARTE_AIRE];

	for (i = 0 ; CARTE_AIRE > i ; i++) {
		nvide  = compte_voisins(cartep, i, DEMO_VIDE);
		nsains = compte_voisins(cartep, i, DEMO_SAINE);
		nvirus = compte_voisins(cartep, i, DEMO_VIRUS);

		type = cartep[i];
		newcarte[i] = simuler_cell(type, nvide, nsains, nvirus);
	}

	memcpy(cartep, newcarte, CARTE_AIRE);
}

/**
 * Simuler des jours d'evolution
 *
 * Simule @njours d'évolution de la population dans @cartep.
 * Apres chaque jour, la carte est affichee sur la sortie standard.
 *
 *
 * @param cartep    la carte de simulation
 * @param njours    le nombre de jours a simuler
 * @return          ne retourne rien
 */
static void simuler(char* cartep, size_t njours)
{
	size_t i;

	assert(0 != cartep);


	for (i = 1 ; njours >= i ; i++) {
		simuler_jour(cartep);
		afficher_jourcarte(i, cartep);
	}
}

/* -------------------------------------------------------------------------- */

/**
 * Determine si l'argument est une nombre acceptable
 *
 * Determine si @str est un nombre acceptable; soit
 * selon l'expression reguliere (ERE):
 *
 *     ^(-[0-9]+|[0-9]+)
 *
 *
 * @param  str    la chaine a evaluer
 * @return        non-zero si l'argument est un nombre acceptable
 */
static int isnum(const char* str)
{
	size_t i;
	size_t l;

	assert(0 != str);


	l = strlen(str);

	i = ('-' == str[0]) ? 1 : 0;
	if ((0 != i) && (1 == l)) {
		return 0;
	}

	for (/**/ ; l > i ; i++) {
		if (0 == isdigit(str[i])) {
			break;
		}
	}

	return (l == i);
}

int main(int argc, const char* const* argv)
{
	long njours;
	char carte[CARTE_AIRE];


	if (0 != lire_carte(stdin, carte)) {
		return 1;
	}

	afficher_jourcarte(0, carte);


	if (2 < argc) {
		eprintf("Attendu un seul argument: le nombre de jours à simuler.");
		return 1;
	} else if (1 == argc) {
		return 0;
	}

	if (0 == isnum(argv[1])) {
		eprintf("L'argument doit être un nombre entre 0 et 100.");
		return 1;
	}

	errno = 0;
	njours = strtol(argv[1], 0, 10);

	if (0 != errno) {
		eprintf("Nombre invalide (%s):", argv[1]);
	} else if ((0 > njours) || (JOURS_MAX < njours)) {
		eprintf("Le nombre de jours à simuler doit être entre 0 et 100.");
		return 1;
	}

	simuler(carte, (size_t)njours);

	return 0;
}

/* ========================================================================== */
