# TP1: `pandemi.c`

## Description

Le programme `pandemic` sert à simuler l'évolution d'un virus au sein d'une
population donnée en situation de pandémie. Il permet de déterminer le moment
(en jours) où une population sera infectée par un virus contagieux et si elle
sera décimée par celui-ci.

Ce travail est remis dans le cadre du cours INF-3135, suivi à l'automne
2017, enseigné par Alexandre Blondin-Massé, à l'Université du Québec à
Montréal (UQAM).

## Auteur

Philippe Grégoire, GREP17108506

## Fonctionnement

Le simulateur utilise la carte démographique fournis par l'utilisateur
pour calculer l'évolution de sa population. En indiquant le nombre de
jours d'intérêts, l'utilisateur peut voir, jour après jour, l'évolution
de la population et du virus.

La carte démographique est une matrice 40x20 représentant l'état d'infection
de la population par un virus X. Chaque cellule de la carte représente une
zone géographique pouvant avoir 3 états:

* Le caractère `H` indique que la zone géographique contient une population
  saine (non infectée par le virus X).
* Le caractère `X` indique que la zone géographique contient une population
  infectée par le virus X.
* Le caractère `.` indique que la zone géographique est sans population (pas
  de population initiale ou population décédée).

### Compilation

Pour exécuter le programme, il est d'abord nécéssaire de le compiler. Pour
ce faire, vous aurez besoin de:

* un compilateur C supportant la norme ISO/IEC 9899:2011 (C11);
* une version du programme `make`.

Une fois ces logiciels disponibles, il suffit d'exécuter la commande

```sh
make
```

à la racine du projet. Une fois terminée, le simulateur sera disponible
à `bin/pandemic`. Un générateur de cartes sera également disponible à
`bin/generator`.

### Exécution

Pour effectuer une simulation, vous aurez besoin d'une carte démographique
(tel que décrit plus haut). Vous pourrez ensuite exécuter le programme de
simulation en effectuant la commande:

```sh
cat carte | ./bin/pandemic 10
```

Pour simuler `10` jours d'évolution de la carte `carte`.

## Tests

En tout temps, il est possible de tester le programme en lançant la commande

```sh
sh ./tests.sh
```

ou à l'aide de la cible `check` du Makefile

```sh
make check
```

### Format des tests

Le script de tests s'occupe de compiler le programme `pandemic` en utilisant le
Makefile puis de l'exécuter avec les tests trouvés dans le répertoire `tests/`.

Le format des cas de tests est le suivant:

* `test_XX.args` nombre de jour à simuler à passer en argument au programme
* `test_XX.in` carte à saisir dans `stdin`
* `test_XX.res` sortie attendue (`stdout` et `stderr` confondus)

### Générateur de cartes

Le programme `generator` permet de générer des cartes de configuration aléatoirement.
Il est très pratique pour tester votre programme.

Usage:

```sh
Usage: generator <lignes> <colonnes>
```

Exemple d'utilisation:

```sh
make bin/generator
bin/generator 5 10
```

Affiche:

```sh
..X...HX..
..XXX...HH
H..XH.H...
.XHXXXXX..
...HXXH.X.
```

## Références

* [Énoncé de travail](https://gitlab.com/ablondin/inf3135-aut2017-tp1-enonce)

## État du projet

Indiquez toutes les tâches qui ont été complétés en insérant un `X` entre les
crochets. Si une tâche n'a pas été complétée, expliquez pourquoi.

* [X] Le nom du dépôt GitLab est exactement `inf3135-aut2017-tp1` (Pénalité de
  **50%**).
* [X] L'URL du dépôt GitLab est exactement (remplacer `utilisateur` par votre
  nom identifiant GitLab) `https://gitlab.com/utilisateur/inf3135-aut2017-tp1`
  (Pénalité de **50%**).
* [X] L'utilisateur `ablondin` (groupe 20) ou `Morriar` (groupe 40) a accès au
  projet en mode *Developer* (Pénalité de **50%**).
* [X] Le dépôt GitLab est privé (Pénalité de **50%**).
* [X] Le fichier `.gitignore` a été mis à jour.
* [X] Le fichier `pandemi.c` a été implémenté.
* [X] La cible `bin/pandemic` a été ajoutée dans le Makefile.
* [X] Aucun test exécuté avec la commande `make check` n'échoue.
* [X] Les sections incomplètes du fichier `README.md` ont été complétées.
